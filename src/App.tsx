import React, { Suspense, useCallback, useContext, useEffect, useState } from 'react';
import './reset.scss';
import './App.scss';
import Switcher from "./components/simple-components/Switcher/Switcher";
import { BrowserRouter as Router, Link, NavLink, Route, Routes } from "react-router-dom";
import Characters from "./pages/Characters";
import Spells from "./pages/Spells";
import Books from "./pages/Books";
import Game from "./pages/Game";
import NotFound from "./pages/NotFound";
import Home from "./pages/Home";
import { DataContext, IData } from "./context/DataContext";
import { IUserSettings, UserSettingsContext } from "./context/UserSettingsContext";
import ButtonGroup, { IOption } from "./components/simple-components/ButtonGroup/ButtonGroup";
import i18n from './i18n';
import LocaleContext from './context/LocaleContext';
import { DarkThemeContext, DarkThemeProvider } from "./context/ThemeContext";
import PageWrapper from "./components/composite-components/PageWrapper/PageWrapper";
import Navigation from "./components/composite-components/Navigation/Navigation";

function App() {
  const [locale, setLocale] = useState(i18n.language);
  const [isLoading, setIsLoading] = useState(false);
  const [loadedData, setLoadedData] = useState<IData>({});
  const [userSettings, setUserSettings] = useState<IUserSettings>({});

  i18n.on('languageChanged', (lng) => setLocale(i18n.language));


  const fetchData = useCallback(async () => {
    Promise.all([fetchBooks(), fetchCharacters(), fetchSpells()])
      .then((res) => {
        setLoadedData({
          books: res[0],
          characters: res[1],
          spells: res[2]
        })
      })
      .catch((err) => console.log(err))
  }, []);

  const fetchBooks = async () => {
    const books = await fetch('https://fedeperin-harry-potter-api-en.herokuapp.com/books/')
    return books.json();
  }

  const fetchSpells = async () => {
    const spells = await fetch('https://fedeperin-harry-potter-api-en.herokuapp.com/spells/')
    return spells.json();
  }

  const fetchCharacters = async () => {
    const char = await fetch('https://fedeperin-harry-potter-api-en.herokuapp.com/characters/')
    return char.json();
  }

  useEffect(() => {
    setIsLoading(true);
    fetchData()
      .then(() => setIsLoading(false))
      .catch(err => console.log(err))
  }, [])

  const handleOptionClick = (optId: number) => {
    const lng = langOptions.find(op => op.id === optId);
    if (locale !== lng?.label ) {
      i18n.changeLanguage(lng?.label);
    }
  }

  const langOptions = [{
    id: 0,
    label: "en"
    },
    {
      id: 1,
      label: "est"
    },
    {
      id: 2,
      label: "ru"
    },
  ]


  return (
    <UserSettingsContext.Provider value={userSettings}>
      <DataContext.Provider value={loadedData}>
        {/*TODO: fix types*/}
        {/* @ts-ignore*/}
        <LocaleContext.Provider value={{locale, setLocale}}>
          <DarkThemeProvider>
            <div className="App">
              <Suspense fallback={"Loading"}>
                {isLoading ? "is loading..." :
                  <Router>
                    <PageWrapper>
                      <>
                        <div style={{
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'center',
                          minWidth: '100px',
                          position: 'absolute',
                          zIndex: 1,
                          bottom: '20px',
                          right: '20px',
                          backgroundColor: 'inherit'
                        }}>
                          <Switcher/>
                          <ButtonGroup active={langOptions.find(op => op.label === i18n.language) as IOption} options={langOptions} onOptionClick={handleOptionClick} />
                        </div>

                        <Navigation />

                      </>
                      <Routes>
                        {/*TODO for v1.0.1:
                        1. welcome page
                        2. characters page
                        3. books page
                      */}
                        {/*<Route index element={<Home />} />*/}
                        {/*<Route path={"characters"} element={<Characters />} />*/}
                        <Route path={"spells"} element={<Spells />} />
                        {/*<Route path={"books"} element={<Books />} />*/}
                        <Route index element={<Game />} />
                        <Route path={"*"} element={<NotFound />} />
                      </Routes>
                    </PageWrapper>

                  </Router>
                }
              </Suspense>
            </div>
          </DarkThemeProvider>
        </LocaleContext.Provider>
      </DataContext.Provider>
    </UserSettingsContext.Provider>
  );
}

export default App;
