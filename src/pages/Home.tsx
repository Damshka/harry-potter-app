import { useContext } from "react";
import { DataContext } from "../context/DataContext";

const Home = () => {
  const data = useContext(DataContext);

  console.log(data);
  return <>Home</>
}

export default Home;
