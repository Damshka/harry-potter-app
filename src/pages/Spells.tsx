import { useContext, useState } from "react";
import { DataContext } from "../context/DataContext";
import Grid from "../components/simple-components/Grid/Grid";
import Card from "../components/simple-components/Card/Card";
import SpellCard from "../components/composite-components/SpellCard/SpellCard";
import { useTranslation } from "react-i18next";

const Spells = () => {
  const { t } = useTranslation();
  const data = useContext(DataContext);
  const favs = localStorage.getItem("favs")?.split(",");
  const [favSpells, setFavSpells] = useState(favs || []);

  const handleCardClick = (id: number) => {
    let favs = []
    if (favSpells.includes(id.toString())) {
      favs = favSpells.filter(i => i !== id.toString())
    } else {
      favs = [...favSpells, id.toString()];
    }
    setFavSpells(favs);
    localStorage.setItem('favs', favs.join(","));
  }

  const label = (id: number) => favSpells.includes(id.toString()) ? t("removeFavs") : t("addFavs");

  return <Grid>
    {data.spells?.map((spell) => (
      <Card key={spell.id}>
        <SpellCard
          buttonLabel={label(spell.id)}
          fav={favSpells.includes(spell.id.toString())}
          id={spell.id}
          spell={spell.spell}
          use={spell.use}
          onClick={handleCardClick} />
      </Card>
      ))}
  </Grid>
}

export default Spells;
