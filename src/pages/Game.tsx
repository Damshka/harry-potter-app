import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { DataContext } from "../context/DataContext";
import Button from "../components/simple-components/Button/Button";
import { TCharacter } from "../types/character";
import { TSpell } from "../types/spell";
import Card from "../components/simple-components/Card/Card";
import { H1, H2, P1 } from "../components/simple-components/Typography/Typography";
import { DarkThemeContext } from "../context/ThemeContext";
import Grid from "../components/simple-components/Grid/Grid";
import Flex from "../components/simple-components/Flex/Flex";
import CharCard from "../components/composite-components/CharCard/CharCard";
import SpellCard from "../components/composite-components/SpellCard/SpellCard";

import { ReactComponent as Felix } from "../assets/img/Felix.svg";
import Wrapper from "../components/simple-components/Wrapper/Wrapper";

export interface IData {
  char: TCharacter | undefined,
  spells: TSpell[] | undefined,
  luck: number
}

const Game = () => {
  const { t } = useTranslation();
  const {darkTheme} = useContext(DarkThemeContext);
  const [winner, setWinner] = useState<TCharacter | undefined>(undefined);
  const [luck, setLuck] = useState(false);
  const [luckClicked, setLuckClicked] = useState(false);
  const [isFight, setIsFight] = useState(false);
  const [competitor, setCompetitor] = useState<IData>({
    char: undefined,
    spells: undefined,
    luck: 1
  });
  const [hero, setHero] = useState<IData>({
    char: undefined,
    spells: undefined,
    luck: 1
  });
  const data = useContext(DataContext);


  const generateHero = () => {

    const randHero = Math.round(Math.random() * (data?.characters?.length || 19));
    let randComp = Math.round(Math.random() * (data?.characters?.length || 19));
    while (randComp === randHero) {
      randComp = Math.round(Math.random() * (data?.characters?.length || 19));
    }
    const mainHero: TCharacter = data?.characters?.[randHero - 1] as TCharacter;
    const compHero: TCharacter = data?.characters?.[randComp - 1] as TCharacter;
    setHero({...hero, char: mainHero});
    setCompetitor({...competitor, char: compHero});
  }

  const generateSpells = () => {

    const randHeroSpellIds = [...Array(3)].map((item) => Math.round(Math.random() * (data?.spells?.length || 72)));
    const randCompSpellIds = [...Array(3)].map((item) => Math.round(Math.random() * (data?.spells?.length || 72)));

    const heroSpells = randHeroSpellIds.map(id => id !== 0 ? data?.spells?.[id - 1] as TSpell :  data?.spells?.[id] as TSpell);
    const compSpells = randCompSpellIds.map(id => id !== 0 ? data?.spells?.[id - 1] as TSpell :  data?.spells?.[id] as TSpell);
    setHero({...hero, spells: heroSpells})
    setCompetitor({...competitor, spells: compSpells});
  }

  const generateLuck = () => {
    setLuckClicked(true);
    const isLuck = Math.random();
    if (isLuck > 0.5) {
      const randHero = Math.round(Math.random() * (data?.characters?.length || 100));
      const randComp = Math.round(Math.random() * (data?.characters?.length || 100));
      setHero({...hero, luck: randHero});
      setCompetitor({...competitor, luck: randComp});
      setLuck(true);
    }
  }

  const newFight = () => {
    setHero({
      char: undefined,
      spells: undefined,
      luck: 1
    });
    setCompetitor({
      char: undefined,
      spells: undefined,
      luck: 1
    });
    setLuckClicked(false);
    setLuck(false);
    setIsFight(false);
    setWinner(undefined);
  }

  const fight = () => {
    setIsFight(true);
    const heroSpells = hero.spells?.map(spell => spell.id).reduce((a, b) => a + b) || 1;
    const compSpells = competitor.spells?.map(spell => spell.id).reduce((a, b) => a + b) || 1;

    if (heroSpells * hero.luck > compSpells * competitor.luck) {
      setWinner(hero.char as TCharacter);
    }
    if (heroSpells * hero.luck < compSpells * competitor.luck) {
      setWinner(competitor.char as TCharacter);
    }
    if (heroSpells * hero.luck === compSpells * competitor.luck) {
      setWinner(undefined);
    }
  }

  return <Wrapper>
      <Flex alignment>
        <H1>{t('greeting')}</H1>
        <P1>{t('message')}</P1>
      </Flex>
      <Flex direction={'row'}>
        {!hero.char && <Button label={t('hero')} onClick={generateHero}/>}
        {(!!hero.spells && !luck && !luckClicked && !isFight) &&
            <Button label={t('felix')}
            onClick={generateLuck} />}
        {(!winner && !!hero.spells && !!competitor.spells) && <Button label={t('fight')} onClick={fight}/>}
        {(!!hero.char && !hero.spells) && <Button label={t('spells')} onClick={generateSpells}/>}
        {(!!winner && !!hero.char && !!competitor.char && !!hero.spells && !!hero.spells) && <Button label={t('again')} onClick={newFight} />}
        {isFight  ? !winner ? <H1>{t('draw')}</H1> :  hero.char?.id === winner?.id ? <H1>{t('winner')}</H1> : <H1>{t('loser')}</H1> : ''}
      </Flex>


    <Flex>
      {!!hero.char &&
          <>
            <div style={{width: '100%'}}>
              <div style={{display: 'flex', alignItems: 'center', gap: '20px'}}><H2>{t("you")}</H2> {luck && <span style={{
                display: 'block',
                width: '32px',
                height: '32px',
                fill: 'gold'
              }}>
            <Felix />
              </span>}
                {(!luck && luckClicked) && <span>{t("tip")}</span>}
              </div>
              <hr style={{
                marginBottom: '20px'
              }}/>

              <Grid>
                {!!hero.char &&
                    <Card>
                      <CharCard
                          id={hero.char.id}
                          character={hero.char.character}
                          image={hero.char.image}
                          hogwartsHouse={hero.char.hogwartsHouse}
                      />
                    </Card>}
                {hero?.spells?.map(sp => (
                  <Card key={sp.id}>
                    <SpellCard id={sp.id} spell={sp.spell} use={sp.use} />
                  </Card>
                ))}
              </Grid>
            </div>
            <div style={{width: '100%'}}>
              <H2>{competitor.char?.character}</H2>
              <hr style={{
                marginBottom: '20px'
              }}/>
              <Grid>
                {!!competitor.char && <Card>
                  <CharCard
                      id={competitor.char.id}
                      character={competitor.char.character}
                      image={competitor.char.image}
                      hogwartsHouse={competitor.char.hogwartsHouse}
                  />
                </Card>}
                {competitor?.spells?.map(sp => (
                  <Card key={sp.id}>
                    <SpellCard id={sp.id} spell={sp.spell} use={sp.use} />
                  </Card>
                ))}
              </Grid>
            </div>
          </>
          }
    </Flex>
  </Wrapper>
}

export default Game;
