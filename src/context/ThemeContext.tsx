import { createContext, useState } from "react";

const DarkThemeContext = createContext({
  darkTheme: false,
  toggleDarkTheme: () => {}
});

const DarkThemeProvider = (props: any) => {
  const isDarkTheme = localStorage.getItem('darkTheme');
  const [darkTheme, setDarkTheme] = useState<boolean>(isDarkTheme === "true");

  const toggleDarkTheme = () => {
    localStorage.setItem('darkTheme', JSON.stringify(!darkTheme));
    setDarkTheme(!darkTheme);
  };

  return (
    <div>
      <DarkThemeContext.Provider value={{darkTheme, toggleDarkTheme}}>
        {props.children}
      </DarkThemeContext.Provider>
    </div>
  )
}

export {DarkThemeContext, DarkThemeProvider};
