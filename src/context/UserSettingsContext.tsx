import { createContext } from "react";

export interface IUserSettings {
  lang?: string,
  theme?: "dark" | "light"
}

const userSettings: IUserSettings = {
  lang: "en",
  theme: "light"
}

const UserSettingsContext = createContext(userSettings);

export  {UserSettingsContext}
