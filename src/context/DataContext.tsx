import { createContext } from "react";
import { TSpell } from "../types/spell";
import { TCharacter } from "../types/character";
import { TBook } from "../types/book";

export interface IData {
  spells?: TSpell[] | undefined,
  characters?: TCharacter[] | undefined,
  books?: TBook[] | undefined
}

const data: IData = {};

const DataContext = createContext(data);

export { DataContext };
