export type TSpell = {
  id: number,
  spell: string,
  use: string
}
