export type TBook = {
  id: number,
  title: string,
  releaseDay: string,
  author: string,
  description: string
}
