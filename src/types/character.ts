export type TCharacter = {
  id: number,
  character: string,
  nickname: string,
  hogwartsStudent?: boolean,
  hogwartsHouse?: string,
  interpretedBy?: string,
  child?: string[],
  image?: string
}
