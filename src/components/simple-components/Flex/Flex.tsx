import React from "react";
import classes from "./Flex.module.scss";

export interface FlexProps {
  children: React.ReactNode,
  direction?: 'row' | 'column',
  alignment?: boolean
}

const Flex: React.FC<FlexProps> = ({children, direction, alignment}) => {
  return <div className={`${classes.flex} ${direction === 'row' ? classes.flex__row : ''} ${alignment ? classes.flex__left : ''}`}>{children}</div>
}

export default Flex;
