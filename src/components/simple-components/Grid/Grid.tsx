import React, { useContext } from "react";

import classes from "./Grid.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";

export interface GridProps {
  children: React.ReactNode,
}

const Grid: React.FC<GridProps> = ({children}) => {
  const {darkTheme} = useContext(DarkThemeContext);
  return <section className={`${classes.grid} ${darkTheme ? classes.grid__dark : classes.grid__light}`}>
    {children}
  </section>
}

export default Grid;
