import React, { useContext } from "react";

import classes from "./Card.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";

export interface CardProps {
  children: React.ReactNode,
  kind?: "bordered" | "shadow" | "default"
}

const Card: React.FC<CardProps> = ({children}) => {
  const {darkTheme} = useContext(DarkThemeContext);
  return <article className={`${classes.card} ${darkTheme ? classes.card__dark : classes.card__light}`}>
    {children}
  </article>
}

export default Card;
