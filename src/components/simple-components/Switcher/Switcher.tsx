import React, { useContext } from "react";
import classes from "./Switcher.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";


export interface SwitcherProps {

}

const Switcher: React.FC<SwitcherProps> = (props) => {
  const {darkTheme, toggleDarkTheme} = useContext(DarkThemeContext);
  const toggleTheme = () => {
    toggleDarkTheme();
  }

  return <div className={classes.themeSwitch__container}>
    <input
      checked={darkTheme}
      onChange={toggleTheme}
      type="checkbox"
      id="themeSwitch"
      name="theme-switch"
      className={classes.themeSwitch__input}/>
    <label htmlFor="themeSwitch" className={`${classes.themeSwitch__label} ${darkTheme ? classes.themeSwitch__label_dark : classes.themeSwitch__label_light}`}>
      <span/>
    </label>
  </div>
}

export default Switcher;
