import React, { useContext } from "react";
import classes from "./ButtonGroup.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";

export type IOption = {
  id: number,
  label: string
}

export interface ButtonGroupProps {
  active: IOption,
  options: IOption[],
  onOptionClick: (id: number) => void
}

const ButtonGroup: React.FC<ButtonGroupProps> = ({active, options, onOptionClick}) => {
  const { darkTheme } = useContext(DarkThemeContext);

  return <div className={classes.group__wrapper}>
      {options.map((o) => (
        <button className={`${classes.group__btn} ${darkTheme ? classes.group__btn__dark : classes.group__btn__light} ${active?.id === o.id && (darkTheme ? classes.group__btn__dark_selected : classes.group__btn__light_selected)}`} key={o.id} onClick={() => onOptionClick(o.id)}>{o.label}</button>
      ))}
    </div>
}

export default ButtonGroup;
