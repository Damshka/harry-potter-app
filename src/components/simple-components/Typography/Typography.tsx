import React from "react";
import classes from "./Typography.module.scss";

const H1: React.FC<any> = (props) => {
  return <h1 className={classes.typography}>{props.children}</h1>
}

const H2: React.FC<any> = (props) => {
  return <h2 className={classes.typography}>{props.children}</h2>
}

const P1: React.FC<any> = (props) => {
  return <p className={classes.typography__prgh_default}>{props.children}</p>
}

const P2: React.FC<any> = (props) => {
  return <p className={classes.typography__prgh_small}>{props.children}</p>
}

export {H1, H2, P1, P2};
