import React, { useContext } from "react";

import classes from "./Button.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";

export interface ButtonProps {
  label: string,
  onClick: () => void
}

const Button: React.FC<ButtonProps> = ({label, onClick}) => {
  const { darkTheme } = useContext(DarkThemeContext);

  return <button
    className={`${classes.btn} 
    ${darkTheme ? classes.btn__dark : classes.btn__light}`}
    type={"button"}
    onClick={onClick}>{label}</button>
}

export default Button;
