import React, { useContext } from "react";
import classes from "./Wrapper.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";

const Wrapper: React.FC<any> = ({children}) => {
  const { darkTheme } = useContext(DarkThemeContext);
  return <section className={`${classes.wrapper} ${darkTheme ? classes.wrapper__dark : ''}`}>{children}</section>
}

export default Wrapper;
