import React from "react";
import classes from "./SpellCard.module.scss";
import { H2, P2 } from "../../simple-components/Typography/Typography";
import Button from "../../simple-components/Button/Button";
import { ReactComponent as Wand } from "../../../assets/img/Wand.svg";

export interface SpellCardProps {
  id: number,
  fav?: boolean,
  spell: string,
  use: string,
  onClick?: (id: number) => void,
  buttonLabel?: string
}

const SpellCard: React.FC<SpellCardProps> = ({id, fav, spell, use, onClick, buttonLabel}) => {

  return <div className={classes.spell}>
    {fav && <span className={classes.spell__fav}><Wand /></span>}
    <div>
      <H2>{spell}</H2>
      <P2>{use}</P2>
    </div>
    { !!onClick && <Button label={buttonLabel || "Add to favs"} onClick={() => {
      onClick?.(id)
    }
    }/>}
  </div>
}

export default SpellCard;
