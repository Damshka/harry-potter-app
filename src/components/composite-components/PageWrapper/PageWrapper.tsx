import React, { useContext } from "react";

import classes from "./PageWrapper.module.scss";
import { DarkThemeContext } from "../../../context/ThemeContext";

const PageWrapper: React.FC<any> = ({children}) => {
  const {darkTheme} = useContext(DarkThemeContext);
  return <main className={`${classes.page_wrapper} ${darkTheme ? classes.page_wrapper__dark : classes.page_wrapper_light}`}>
    {children}
  </main>
}

export default PageWrapper;
