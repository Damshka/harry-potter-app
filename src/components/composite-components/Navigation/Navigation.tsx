import { NavLink } from "react-router-dom";
import React, { useContext } from "react";
import { DarkThemeContext } from "../../../context/ThemeContext";
import classes from "./Navigation.module.scss";
import { useTranslation } from "react-i18next";

interface IRoutes {
  route: string,
  label: string
}

const Navigation = () => {
  const {darkTheme} = useContext(DarkThemeContext);
  const { t } = useTranslation();

  const routes: IRoutes[] = [
    {
      route: 'spells',
      label: t('spells')
    },
    {
      route: './',
      label: t('game')
    }
  ];

  return <nav className={classes.nav}>
    {routes.map((r, index) => (
      <NavLink key={index} to={r.route} className={`${classes.nav__link} ${darkTheme ? classes.nav__link_dark : classes.nav__link_light}`} style={({ isActive }) => {
        return {
          ...isActive && {
            color: '#ae0001'
          }
        }
      }}>{r.label}</NavLink>
    ))}
  </nav>
}

export default Navigation;
