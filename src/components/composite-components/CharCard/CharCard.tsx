import React from "react";
import classes from "./CharCard.module.scss";
import { H2, P1 } from "../../simple-components/Typography/Typography";

export interface CharCardProps {
  id: number,
  character: string,
  hogwartsHouse?: string | undefined,
  image?: string
}

const CharCard: React.FC<CharCardProps> = ({character, hogwartsHouse, image}) => {
  return <div className={classes.character}>
    {!!image &&
        <div
            className={`
            ${classes.character__img} 
            ${hogwartsHouse === 'Gryffindor' ? 
              classes.character__img_gr : 
              hogwartsHouse === 'Hufflepuff' ? classes.character__img_hp :
              hogwartsHouse === 'Ravenclaw' ? classes.character__img_rw :
              hogwartsHouse === 'Slytherin' ? classes.character__img_sl : ''
            }`}><img src={image} alt={character}/></div>}
              <H2>{character}</H2>
              {!!hogwartsHouse && <P1>{hogwartsHouse}</P1>}
        </div>
}

export  default CharCard;
